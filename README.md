# Instalation

Add the registry for @pickyfi packages to your _.npmrc_ file:

`@pickyfi:registry=https://gitlab.com/api/v4/packages/npm/`

After that you can install it by executing following command:

`npm i @pickyfi/prices`

# Strucure

[picki.fi](https://app.picky.fi) in it's core consist of a few packages. This package @pickyfi/prices is responsible for calculating and returning the best prices.

# Demo (create-react-app client)

Here is the very basic step-by-step exampe below. Please find the full code example here [example](docs/example.js)

First of all install and import the @taquito library

`npm i @taquito/taquito`

and then import it and configure to work with an RPC

```javascript
const taquito = require('@taquito/taquito');

const RPC_URL = "https://mainnet.api.tez.ie";
const tezos = new taquito.TezosToolkit(RPC_URL);
```

Next step is to fetch the data from storages. For this we can use either additional packages or craft an array manually. In this exaple @pickyfi/tokens will be used. You need to install it as well


`npm i @pickyfi/prices`

and import all supported contracts as

`const { contractAddressToDex } = require("@pickyfi/tokens");`

after getting that most basic but non-efficient method to get the storage is:

```javascript
const getStorage = async () =>
  new Map([
    ...(await Promise.all(
      Array.from(contractAddressToDex.keys()).map((contract) =>
        tezos.contract
          .at(contract)
          .then((dex) => dex.storage())
          .then((storage) => [contract, storage])
      )
    )),
  ]);
```

you can use any other methods as long as it's satisfied the same format.

After getting this you can calculate prices by using the getPrices method imported from @pickyfi/prices

`const { getPrices } = require("../dist/index.js");`

We are getting the best price for kUSD tzBTC pair for 1 kUSD

```javascript
(async () => {
  const contractStorage = await getStorage();
  const prices = getPrices({
    contractStorage,
  })({
    fromTokenAddress: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV", // kUSD
    toTokenAddress: "KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn", // tzBTC
    amount: "1", 
  });
  /* It returns 5 prices, can be configured by maxRouts parameter of getPrices
   * use Infinity to set it to max
   */
  const [bestPrice] = prices;
  const bestPriceRoute = bestPrice.dex.reduce((acc, dex, idx) => {
    acc += idx !== 0 ? " -> " : "";
    acc += `${dex.fromTokenSymbol}/${dex.toTokenSymbol}`;
    return acc;
  }, "");
  console.log(
    `best route is ${bestPriceRoute} with price ${bestPrice.priceWithDecimals}`
  );
})();
```

