import { DEXES } from '@pickyfi/tokens';
import BigNumber from 'bignumber.js';

import { quipuswapStateToPoolsInfo } from './quipuswap/quipu-amm'; // TODO rename
import { tzbtcoriginalStateToPoolsInfo } from './sirius/liquidity-baking';
import { vortexStateToPoolsInfo } from './vortex/vortex-amm';
import { plentyStateToPoolsInfo } from './plenty/plenty-amm';
import { spicyswapStateToPoolsInfo } from './spicyswap/spicy-amm';
import { youvesCfmmStateToPoolsInfo } from './youves/youves-stable';
import { plentyStableStateToPoolsInfo } from './plenty/plenty-stable';

import { liquidityBakingAmountOut } from './market-makers/liquidity-baking-amm';
import { cfmmAmountOut } from './market-makers/cfmm';
import { amm } from './market-makers/amm';

const contractStorageToPoolsExtractors = {
  [DEXES.QUIPUSWAP]: quipuswapStateToPoolsInfo,
  [DEXES.VORTEX]: vortexStateToPoolsInfo,
  [DEXES.PLENTY]: plentyStateToPoolsInfo,
  [DEXES.TZBTCORIGINAL]: tzbtcoriginalStateToPoolsInfo,
  [DEXES.SPICYSWAP]: spicyswapStateToPoolsInfo,
  [DEXES.YOUVES_CFMM]: youvesCfmmStateToPoolsInfo, // TODO consider better naming
  [DEXES.PLENTY_CFMM]: plentyStableStateToPoolsInfo, // TODO consider better naming
};

export const normalizeStateToPool = ({
  contractStorage,
  contractAddressToDex,
}: {
  contractStorage: Map<string, any>; // TODO
  contractAddressToDex: Map<string, any>; // TODO
}) => {
  const regularPools = Array.from(contractStorage.keys()).reduce(
    (acc, address) => {
      const dex = contractAddressToDex.get(address);
      const poolsExtractor = contractStorageToPoolsExtractors[dex];
      // TODO why this is an array?
      // TODO remove forced type after cleanup and try if TypeScript can guess it right?
      const new_pools = poolsExtractor(contractStorage.get(address));

      return [
        ...acc,
        ...new_pools.map(pool => ({
          dex,
          contractAddress: address,
          ...pool,
        })),
      ];
    },
    []
  );

  const invertedPools = regularPools.map(pool => ({
    ...pool,
    address1: pool.address2,
    address2: pool.address1,
    liquidity1: pool.liquidity2,
    liquidity2: pool.liquidity1,
    inverted: true,
    ...(pool.multiplier2
      ? {
          multiplier1: pool.multiplier2,
        }
      : {}),
    ...(pool.multiplier1
      ? {
          multiplier2: pool.multiplier1,
        }
      : {}),
  }));
  return [...regularPools, ...invertedPools].filter(
    p => !(p.liquidity1.toString() === '0' && p.liquidity2.toString() === '0')
  );
};

// TODO add testcase specifically for Liquidity Baking
export const estimatePoolAmountOut = (
  amount: string,
  pool: any, // TODO not a pool change name! // ReturnType<typeof normalizeStateToPool>[0]
  slippageTolerance = 0
) => {
  const amountIn = new BigNumber(amount);
  const slippage = new BigNumber(slippageTolerance / 100); 
  let amountOut: BigNumber;

  switch (pool.dex) {
    case DEXES.TZBTCORIGINAL: {
      amountOut = liquidityBakingAmountOut(pool, amountIn);
      break;
    }
    case DEXES.PLENTY_CFMM:
    case DEXES.YOUVES_CFMM: {
      amountOut = cfmmAmountOut(pool, amountIn);
      break;
    }
    default: {
      amountOut = amm(pool, amountIn);
      break;
    }
  }

  const minAmountOut = amountOut
    .times(new BigNumber(1).minus(slippage))
    .integerValue();

  return { amountOut: amountOut.integerValue(), minAmountOut };
};

