import BigNumber from "bignumber.js";

import { DEXES } from "@pickyfi/tokens"; // TODO this can be removed and hardcoded
import { estimatePoolAmountOut } from "./calculations";

describe("estimatePoolAmountOut", () => {
  test("swap 67 XTZ to 89 kUSD", async () => {
    expect(
      estimatePoolAmountOut("67000000", {
        address1: "address1",
        address2: "address2",
        liquidity1: new BigNumber("207567136070"),
        liquidity2: new BigNumber("2.7807211181952784e+23"),
        fee1: new BigNumber("0.997"),
        fee2: new BigNumber("1"),
        dex: DEXES.SPICYSWAP,
      }).amountOut.toNumber()
    ).toEqual(89460034513002120000);
  });
  test("swap 89 kUSD to kUSD", async () => {
    expect(
      estimatePoolAmountOut("89000000000000000000", {
        address1: "address2",
        address2: "address1",
        liquidity1: new BigNumber("2.8644565966686688e+23"),
        liquidity2: new BigNumber("201540028122"),
        fee1: new BigNumber("0.997"),
        fee2: new BigNumber("1"),
        dex: DEXES.SPICYSWAP,
      }).amountOut.toNumber()
    ).toEqual(62412233);
  });
});

describe("estimatePoolAmountOut", () => {
  test("swap 67 uUSD to 89 kUSD from YOUVES_CFMM", async () => {
    expect(
      estimatePoolAmountOut("67000000", {
        address1: "address1",
        address2: "address2",
        liquidity1: new BigNumber("84127371311060986"),
        liquidity2: new BigNumber("4.611143362527092766252e+22"),
        multiplier1: new BigNumber("1000000"),
        multiplier2: new BigNumber("1"),
        fee1: new BigNumber("0.9985"),
        dex: DEXES.YOUVES_CFMM,
      }).amountOut.toNumber()
    ).toEqual(66875348083200);
  });
  test("swap 89 kUSD to uUSD", async () => {
    expect(
      estimatePoolAmountOut("89000000000000000000", {
        address1: "address2",
        address2: "address1",
        liquidity1: new BigNumber("4.611143362527092766252e+22"),
        liquidity2: new BigNumber("84127371311060986"),
        multiplier2: new BigNumber("1000000"),
        multiplier1: new BigNumber("1"),
        fee1: new BigNumber("0.9985"),
        dex: DEXES.YOUVES_CFMM,
      }).amountOut.toNumber()
    ).toEqual(88898072770346)
  });
});

