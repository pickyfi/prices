import BigNumber from 'bignumber.js';
import { estimatePoolAmountOut } from './calculations';

import {
  tokenAddressToSymbol, // TODO can be moved back???
  getDecimals,
  getImageFromContract,
} from '@pickyfi/tokens';

export const getPricesForRoutes = (
  amountIn: BigNumber,
  routes,
  fromTokenAddress: string,
  toTokenAddress: string,
  slippageTolerance: number
) =>
  routes.reduce((acc, route) => {
    /* const price = pricesOfThePaths({ */
    /*   paths: route, //TODO //FIXME stick to naming */
    /*   amountIn, */
    /* }); */
    const price = route.reduce(
      (amount, hop) => estimatePoolAmountOut(amount, hop, 0).amountOut,
      amountIn
    );

    const minAmountOut = route.reduce(
      (amount, path) =>
        estimatePoolAmountOut(amount, path, slippageTolerance).minAmountOut,
      amountIn
    );

    const newEl = {
      route: route.reduce((acc, currentRoute) => {
        const amountI =
          acc.length === 0 ? amountIn : acc[acc.length - 1].amountOut;
        // TODO //FIXME why toFixed()
        const { amountOut, minAmountOut } = estimatePoolAmountOut(
          amountI.toFixed(),
          currentRoute,
          slippageTolerance
        );
        const route = {
          ...currentRoute,
          amountIn: amountI,
          amountOut,
          minAmountOut,
        };
        return acc.concat(route);
      }, []),
      address1: fromTokenAddress, // TODO fix it in integration
      address2: toTokenAddress, // TODO fix it in integration
      decimals1: getDecimals(fromTokenAddress),
      decimals2: getDecimals(toTokenAddress),
      amount: amountIn,
      minAmountOut,
      dex: route.reduce((acc, route) => {
        acc = acc.concat({
          dex: route.dex,
          fromTokenImage: getImageFromContract(route.address1),
          toTokenImage: getImageFromContract(route.address2),
          fromTokenSymbol: tokenAddressToSymbol(route.address1),
          toTokenSymbol: tokenAddressToSymbol(route.address2),
          fromTokenAddress: route.address1,
          toTokenAddress: route.address2,
        });
        return acc;
      }, []),
      price,
    };
    acc = [...acc, newEl];
    return acc;
  }, []);

export const getAmountWithDecimals = (
  amount: number,
  fromTokenAddress: string
) =>
  new BigNumber(amount).times(
    new BigNumber(10).exponentiatedBy(getDecimals(fromTokenAddress))
  );

const getRoutesAfterTrade = ({
  routes,
  amountIn,
}: {
  routes;
  amountIn: BigNumber;
}) =>
  routes.reduce((acc, dex, idx, arr) => {
    const amountPoolIn = idx === 0 ? amountIn : arr[idx - 1].liquidity2;
    const amountOut = estimatePoolAmountOut(amountPoolIn.toString(), dex)
      .amountOut;

    const poolAfterOperation = {
      ...dex,
      liquidity1: dex.liquidity1.plus(amountIn),
      liquidity2: dex.liquidity2.minus(amountOut),
    };
    acc = acc.concat(poolAfterOperation);
    return acc;
  }, []);

export const getPriceImpact = (
  amountIn: BigNumber,
  routes,
  fromTokenAddress: string,
  toTokenAddress: string,
  slippageTolerance: number
) => {
  const priceBefore = getPricesForRoutes(
    getAmountWithDecimals(1, fromTokenAddress),
    [routes],
    fromTokenAddress,
    toTokenAddress,
    slippageTolerance
  )[0].price;

  const routesAfterTrade = getRoutesAfterTrade({ routes, amountIn });

  const priceAfter = getPricesForRoutes(
    getAmountWithDecimals(1, fromTokenAddress),
    [routesAfterTrade],
    fromTokenAddress,
    toTokenAddress,
    slippageTolerance
  )[0].price;

  return priceAfter.div(priceBefore).toNumber();
};
