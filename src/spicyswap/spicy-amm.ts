import BigNumber from "bignumber.js";
import { getAddressBasedOnTokenType } from "@pickyfi/tokens";

import { Pool } from "../index";

export interface SpicyStorage {
  token1: {
    token_id: string;
    fa2_address: string;
  };
  token0: {
    token_id: string;
    fa2_address: string;
  };
  reserve0: string;
  reserve1: string;
}

export const spicyswapStateToPoolsInfo = (storage: SpicyStorage) : Pool[] => [
  {
    address1: Boolean(storage.token0.token_id)
      ? getAddressBasedOnTokenType({
          tokenType: "FA2",
          address: storage.token0.fa2_address,
          tokenId: storage.token0.token_id,
        })
      : getAddressBasedOnTokenType({
          tokenType: "FA12",
          address: storage.token0.fa2_address,
        }),
    address2: Boolean(storage.token1.token_id)
      ? getAddressBasedOnTokenType({
          tokenType: "FA2",
          address: storage.token1.fa2_address,
          tokenId: storage.token1.token_id,
        })
      : getAddressBasedOnTokenType({
          tokenType: "FA12",
          address: storage.token1.fa2_address,
        }),
    liquidity1: new BigNumber(storage.reserve0),
    liquidity2: new BigNumber(storage.reserve1),
    fee1: new BigNumber("0.997"),
    fee2: new BigNumber("1"),
  },
];
