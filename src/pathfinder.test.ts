import { getRoutes } from "./pathfinder";
import { Pool } from './index';

const invertPools = (pools: Pool[]) =>
  pools.concat(
    pools.map((pool) => ({
      ...pool,
      address1: pool.address2,
      address2: pool.address1,
    }))
  );

describe("getRoutes from mock data structure", () => {
  test("1", () => {
    const pools = [
      {
        address1: "xtz",
        address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
      },
      {
        address1: "xtz",
        address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
      },
      {
        address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
      },
      {
        address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
      },
      {
        address1: "xtz",
        address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
      },
      {
        address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
      },
      {
        address1: "xtz",
        address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
      },
      {
        address1: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        address2: "xtz",
      },
      {
        address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        address2: "xtz",
      },
    ];
    expect(
      getRoutes({
        pools: invertPools(pools as unknown as Pool[]),
        fromTokenAddress: "xtz",
        toTokenAddress: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
      })
    ).toEqual([
      [
        {
          address1: "xtz",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        },
        {
          address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        },
        {
          address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        },
        {
          address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        },
        {
          address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        },
        {
          address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        },
        {
          address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        },
        {
          address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        },
        {
          address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        },
        {
          address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
      [
        {
          address1: "xtz",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
        },
        {
          address1: "KT1A5P4ejnLix13jtadsfV9GCnXLMNnab8UT",
          address2: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
        },
        {
          address1: "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb",
          address2: "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
        },
      ],
    ]);
  });
});
