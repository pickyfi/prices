import BigNumber from "bignumber.js";

import { Pool } from "../index"

export const amm = (pool: Pool, amountIn: BigNumber) => {
  const amountOut = pool.fee1
    .multipliedBy(pool.fee2)
    .multipliedBy(pool.liquidity2)
    .multipliedBy(amountIn)
    .idiv(pool.liquidity1.plus(pool.fee1.multipliedBy(amountIn)));
  return amountOut;
};
