import BigNumber from "bignumber.js";

import { Pool } from "../index"

const LIQUIDITY_BAKING_SUBSIDY = new BigNumber(2500000);

const creditSubsidy = (xtzPool: BigNumber): BigNumber => {
  return xtzPool.plus(LIQUIDITY_BAKING_SUBSIDY);
};

export const liquidityBakingAmountOut = (pool: Pool, amountIn: BigNumber) => {
  let numerator: BigNumber;
  let denominator: BigNumber;

  if (!pool.inverted) {
    numerator = amountIn.times(pool.liquidity2).times(new BigNumber(998001));
    denominator = creditSubsidy(pool.liquidity1)
      .times(new BigNumber(1000000))
      .plus(amountIn.times(new BigNumber(998001)));
  } else {
    numerator = amountIn.times(pool.liquidity2).times(new BigNumber(998001));
    denominator = creditSubsidy(pool.liquidity1)
      .times(new BigNumber(1000000))
      .plus(amountIn.times(new BigNumber(999000)));
  }
  const amountOut = numerator
    .dividedBy(denominator)
    .decimalPlaces(0, BigNumber.ROUND_DOWN);

  return amountOut;
};
