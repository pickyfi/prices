import BigNumber from "bignumber.js";

import { Pool } from "../index"

type NewtonParam = {
  x: BigNumber;
  y: BigNumber;
  dx: BigNumber;
  dy: BigNumber;
  u: BigNumber;
  n: number;
};

const util = (x: BigNumber, y: BigNumber) => {
  const plus = x.plus(y);
  const minus = x.minus(y);

  return [
    plus.exponentiatedBy(8).minus(minus.exponentiatedBy(8)),
    minus.exponentiatedBy(7).plus(plus.exponentiatedBy(7)).multipliedBy(8),
  ];
};

const newton = (p: NewtonParam): BigNumber => {
  if (p.n === 0) return p.dy;
  else {
    const [new_u, new_du_dy] = util(p.x.plus(p.dx), p.y.minus(p.dy));

    //  new_u - p.u > 0 because dy remains an underestimate
    // dy is an underestimate because we start at 0 and the utility curve is convex
    p.dy = p.dy.plus(new_u.minus(p.u).dividedBy(new_du_dy));
    p.n -= 1;

    return newton(p);
  }
};

export const cfmmAmountOut = (pool: Pool, amountIn: BigNumber) => {
  const tokenMultiplierX = new BigNumber(pool.multiplier1);
  const tokenMultiplierY = new BigNumber(pool.multiplier2);

  const x = pool.liquidity1.times(tokenMultiplierX);
  const y = pool.liquidity2.times(tokenMultiplierY);

  const [u] = util(x, y);

  const p: NewtonParam = {
    x: x,
    y: y,
    dx: amountIn.times(tokenMultiplierX),
    dy: new BigNumber(0),
    u: u,
    n: 5,
  };

  const amountOut = newton(p)
    .idiv(tokenMultiplierY)
    .times(pool.fee1)
    .decimalPlaces(0, BigNumber.ROUND_DOWN);
  return amountOut;
};
