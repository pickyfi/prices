import BigNumber from "bignumber.js";
import { getAddressBasedOnTokenType } from "@pickyfi/tokens";

import { Pool } from '../index';

export interface YouvesCFMMStorage {
  tokenPool: string;
  cashPool: string;
  tokenAddress: string;
  cashAddress: string;
  tokenId: string; // TODO is it?
  cashId: string; // TODO is it?
  tokenMultiplier: string;
  cashMultiplier: string;
  feeRatio: {
    numerator: string;
    denominator: string;
  };
}

export const youvesCfmmStateToPoolsInfo = (storage: YouvesCFMMStorage): Pool[] => [
  {
    address1: Boolean(storage.tokenId)
      ? getAddressBasedOnTokenType({
          tokenType: "FA2",
          address: storage.tokenAddress,
          tokenId: storage.tokenId,
        })
      : getAddressBasedOnTokenType({
          tokenType: "FA12",
          address: storage.tokenAddress,
        }),
    address2: Boolean(storage.cashId)
      ? getAddressBasedOnTokenType({
          tokenType: "FA2",
          address: storage.cashAddress,
          tokenId: storage.cashId,
        })
      : getAddressBasedOnTokenType({
          tokenType: "FA12",
          address: storage.cashAddress,
        }),
    liquidity1: new BigNumber(storage.tokenPool),
    liquidity2: new BigNumber(storage.cashPool),
    multiplier1: new BigNumber(storage.tokenMultiplier),
    multiplier2: new BigNumber(storage.cashMultiplier),
    fee1: new BigNumber( // 0.9985
      new BigNumber(storage.feeRatio.numerator).div(
        new BigNumber(storage.feeRatio.denominator)
      )
    ),
  },
];

