import BigNumber from "bignumber.js";
import { getAddressBasedOnTokenType } from "@pickyfi/tokens";

import { Pool } from '../index';

export interface QuipuswapStorage {
  storage: {
    token_id: string;
    token_address: string;
    tez_pool: string;
    token_pool: string;
  };
}

export const quipuswapStateToPoolsInfo = (storage: QuipuswapStorage) : Pool[] => [
  {
    address1: getAddressBasedOnTokenType({ tokenType: "XTZ" }),
    address2: Boolean(storage.storage.token_id)
      ? getAddressBasedOnTokenType({
          tokenType: "FA2",
          address: storage.storage.token_address,
          tokenId: storage.storage.token_id,
        })
      : getAddressBasedOnTokenType({
          tokenType: "FA12",
          address: storage.storage.token_address,
        }),
    liquidity1: new BigNumber(storage.storage.tez_pool),
    liquidity2: new BigNumber(storage.storage.token_pool),
    fee1: new BigNumber("0.997"),
    fee2: new BigNumber("1"),
  },
];
