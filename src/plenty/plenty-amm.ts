import BigNumber from "bignumber.js";
import { getAddressBasedOnTokenType } from "@pickyfi/tokens";

import { Pool } from '../index';

export interface PlentyStorage {
  token1Id: string;
  token1Check: boolean;
  token2Id: string;
  token2Check: boolean;
  token1Address: string;
  token2Address: string;
  token1_pool: string;
  token2_pool: string;
}

export const plentyStateToPoolsInfo = (storage: PlentyStorage) : Pool[] => [
  {
    address1: Boolean(storage.token1Check)
      ? getAddressBasedOnTokenType({
          tokenType: "FA2",
          address: storage.token1Address,
          tokenId: storage.token1Id,
        })
      : getAddressBasedOnTokenType({
          tokenType: "FA12",
          address: storage.token1Address,
        }),
    address2: Boolean(storage.token2Check)
      ? getAddressBasedOnTokenType({
          tokenType: "FA2",
          address: storage.token2Address,
          tokenId: storage.token2Id,
        })
      : getAddressBasedOnTokenType({
          tokenType: "FA12",
          address: storage.token2Address,
        }),
    liquidity1: new BigNumber(storage.token1_pool),
    liquidity2: new BigNumber(storage.token2_pool),
    fee1: new BigNumber("0.9965"),
    fee2: new BigNumber("1"),
  },
];
