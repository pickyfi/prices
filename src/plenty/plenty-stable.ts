import BigNumber from "bignumber.js";
import { getAddressBasedOnTokenType } from "@pickyfi/tokens";

import { Pool } from "../index";

export interface PlentyStableStorage {
  token1Id: string;
  token1Check: boolean;
  token2Id: string;
  token2Check: boolean;
  token1Address: string;
  token2Address: string;
  token1Pool: string;
  token2Pool: string;
  token1Precision: string;
  token2Precision: string;
}

export const plentyStableStateToPoolsInfo = (storage: PlentyStableStorage): Pool[] => [
  {
    address1: Boolean(storage.token1Check)
      ? getAddressBasedOnTokenType({
          tokenType: "FA2",
          address: storage.token1Address,
          tokenId: storage.token1Id,
        })
      : getAddressBasedOnTokenType({
          tokenType: "FA12",
          address: storage.token1Address,
        }),
    address2: Boolean(storage.token2Check)
      ? getAddressBasedOnTokenType({
          tokenType: "FA2",
          address: storage.token2Address,
          tokenId: storage.token2Id,
        })
      : getAddressBasedOnTokenType({
          tokenType: "FA12",
          address: storage.token2Address,
        }),
    liquidity1: new BigNumber(storage.token1Pool),
    liquidity2: new BigNumber(storage.token2Pool),
    multiplier1: new BigNumber(storage.token1Precision),
    multiplier2: new BigNumber(storage.token2Precision),
    fee1: new BigNumber("0.9990"),
  },
];
