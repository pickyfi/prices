import BigNumber from 'bignumber.js';
import { getAddressBasedOnTokenType } from '@pickyfi/tokens';

import { Pool } from '../index';

export interface VortexStorage {
  tokenId: string;
  tokenAddress: string;
  xtzPool: string;
  tokenPool: string;
}

export const vortexStateToPoolsInfo = (storage: VortexStorage): Pool[] => [
  {
    address1: getAddressBasedOnTokenType({ tokenType: 'XTZ' }),
    address2: Boolean(storage.tokenId)
      ? getAddressBasedOnTokenType({
          tokenType: 'FA2',
          address: storage.tokenAddress,
          tokenId: storage.tokenId,
        })
      : getAddressBasedOnTokenType({
          tokenType: 'FA12',
          address: storage.tokenAddress,
        }),
    liquidity1: new BigNumber(storage.xtzPool),
    liquidity2: new BigNumber(storage.tokenPool),
    fee1: new BigNumber('0.9972'),
    fee2: new BigNumber('1'),
  },
];
