import BigNumber from 'bignumber.js';
import { getPricesForRoutes } from './prices';

const ZERO = new BigNumber(0);

const isUnique = (route, existedArray) =>
  Array.from(Array(route.length).keys()).reduce((isUniqueAccumulator, idx) => {
    const un = existedArray.flat().find(stepFromAccRoute => {
      const result =
        stepFromAccRoute.contractAddress === route[idx].contractAddress;
      return result;
    });
    isUniqueAccumulator = isUniqueAccumulator && !Boolean(un);
    return isUniqueAccumulator;
  }, true);

const sortByPrice = (
  amountIn: BigNumber,
  fromTokenAddress: string,
  toTokenAddress: string
) => (a, b) =>
  getPricesForRoutes(amountIn, [b], fromTokenAddress, toTokenAddress, 0)[0]
    .price.minus(
      getPricesForRoutes(amountIn, [a], fromTokenAddress, toTokenAddress, 0)[0]
        .price
    )
    .toNumber();

const routeWeightFn = route => route[0].liquidity1;

const getTotalWeight = unique =>
  unique.reduce((fullAcc, route) => {
    const weight = routeWeightFn(route); // TODO can be written in 1 line?

    fullAcc = fullAcc.plus(weight);
    return fullAcc;
  }, new BigNumber(0));

const getAllAggregationVariants = ({
  amountIn,
  routes,
  fromTokenAddress,
  toTokenAddress,
  slippageTolerance,
}: {
  amountIn: BigNumber;
  routes;
  fromTokenAddress: string;
  toTokenAddress: string;
  slippageTolerance: number;
}) => {
  const totalWeight = getTotalWeight(routes);
  return routes
    .map(route => {
      const weightedBy = routeWeightFn(route).div(totalWeight);

      const weightedAmountIn = amountIn.times(weightedBy).integerValue()

      const currentPrice = getPricesForRoutes(
        weightedAmountIn,
        [route],
        fromTokenAddress,
        toTokenAddress,
        slippageTolerance / routes.length
      );

      return currentPrice;
    })
    .flat()
    .reduce(
      (acc, el) => ({
        ...acc,
        ...el,
        amount: amountIn,
        route: el.route.concat(acc?.route || []),
        isAggregated: true,
        price: acc.price.plus(el?.price || ZERO),
        minAmountOut: acc.minAmountOut.plus(el?.minAmountOut || ZERO),
        percentage: acc.percentage.concat(el?.percentage || []),
        dex: acc.dex.concat([el?.dex] || []),
      }),
      { dex: [], price: ZERO, minAmountOut: ZERO, percentage: [] }
    );
};

const allUniquVariants = (routes, sortBy: (a: any, b: any) => number) =>
  [...routes]
    .sort(sortBy)
    .reduce((acc, route, route3idx) => {
      if (route3idx === 0) {
        acc = acc.concat([route]);
        return acc;
      }

      acc = isUnique(route, acc) ? acc.concat([route]) : acc;
      return acc;
    }, [])
    .reduce((acc, _, idx, arr) => {
      acc = acc.concat([arr.slice(0, idx + 1)]);
      return acc;
    }, [])
    .filter(routes => routes.length > 1);

export const getAggregatedVariants = ({
  amountIn,
  routes,
  fromTokenAddress,
  toTokenAddress,
  slippageTolerance,
}: {
  amountIn: BigNumber;
  routes: any[]; // TODO
  fromTokenAddress: string;
  toTokenAddress: string;
  slippageTolerance: number;
}) => {
  return allUniquVariants(
    routes,
    sortByPrice(amountIn, fromTokenAddress, toTokenAddress)
  )
    .map(routes =>
      getAllAggregationVariants({
        amountIn,
        routes,
        fromTokenAddress,
        toTokenAddress,
        slippageTolerance,
      })
    )
    .flat();
};
