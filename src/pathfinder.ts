export const getRoutes = ({
  pools,
  fromTokenAddress,
  toTokenAddress,
  depth = 4,
}: {
  pools: any;
  fromTokenAddress: string;
  toTokenAddress: string;
  depth?: number;
}) => {
  const getRec = (
    step = 0,
    allPaths: typeof pools,
    filtered = []
  ): typeof pools => {
    if (step === 0) {
      const out = allPaths
        .filter(
          pool =>
            pool.address1 === fromTokenAddress &&
            pool.address2 !== toTokenAddress
        )
        .map(el => [[el]]);

      const filtered = allPaths
        .filter(
          pool =>
            pool.address1 === fromTokenAddress &&
            pool.address2 === toTokenAddress
        )
        .map(el => [el]);
      return getRec(1, out, filtered);
    } else if (step < depth) {
      const out = allPaths
        .map(path =>
          path.map(route =>
            pools.reduce(
              (acc, pool) => {
                if (
                  route[route.length - 1].address2 === pool.address1 &&
                  pool.address1 !== fromTokenAddress
                ) {
                  if (pool.address2 === toTokenAddress) {
                    filtered.push([...route, pool]);
                    return acc;
                  } else {
                    return acc.concat([[...route, pool]]);
                  }
                }
                return acc;
              },
              ['TODO']
            )
          )
        )
        .flat();

      return getRec(step + 1, out, filtered);
    } else {
      return filtered;
    }
  };
  return getRec(0, pools);
};
