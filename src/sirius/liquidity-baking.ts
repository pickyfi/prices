import BigNumber from "bignumber.js";
import { getAddressBasedOnTokenType } from "@pickyfi/tokens";

import { Pool } from '../index';

export interface LiquidityBakingStorage {
  tokenAddress: string;
  xtzPool: string;
  tokenPool: string;
}

export const tzbtcoriginalStateToPoolsInfo = (
  storage: LiquidityBakingStorage
) : Pool[] => [
  {
    address1: getAddressBasedOnTokenType({ tokenType: "XTZ" }),
    address2: getAddressBasedOnTokenType({
      tokenType: "FA12",
      address: storage.tokenAddress,
    }),
    liquidity1: new BigNumber(storage.xtzPool),
    liquidity2: new BigNumber(storage.tokenPool),
    fee1: new BigNumber("999000"),
    fee2: new BigNumber("999000"),
  },
];
