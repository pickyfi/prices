import { getPrices } from './index';
import { mockStorage } from './contractStorageMock';

const storage = mockStorage;

describe('getPricesWithRoots', () => {
  test('return prices array for token - xtz pair', async () => {
    const fromTokenAddress = 'KT1TwzD6zV3WeJ39ukuqxcfK2fJCnhvrdN1X';
    const toTokenAddress = 'xtz';
    expect(
      getPrices({
        contractStorage: new Map([...(mockStorage as any)]), //FIXME
        includeAggregation: false,
      })({
        fromTokenAddress,
        toTokenAddress,
        amount: '12000',
        slippageTolerance: 0,
      })
    ).toMatchSnapshot();
  });
});

describe.skip('getPricesWithRoots with aggregation', () => {
  test('return prices array for token - xtz pair', async () => {
    const fromTokenAddress = 'KT1TwzD6zV3WeJ39ukuqxcfK2fJCnhvrdN1X';
    const toTokenAddress = 'xtz';
    expect(
      getPrices({
        contractStorage: new Map([...(storage as any)]), //FIXME
      })({
        fromTokenAddress,
        toTokenAddress,
        amount: '12000',
        slippageTolerance: 0,
      })
    ).toMatchSnapshot();
  });
});
