import BigNumber from 'bignumber.js';
import {
  contractAddressToDex, // TODO can be moved back???
  applyDecimalsToToken,
  convertTokenToDecimals, // TODO change to this
  DEXES,
} from '@pickyfi/tokens';

import { normalizeStateToPool } from './calculations';
import { getRoutes } from './pathfinder';
import { getPricesForRoutes } from './prices';
import { getAggregatedVariants } from './aggregation';

const MAX_RESULTS = parseInt(process.env.MAX_PATHS || '5', 10) || 5;

export interface Pool {
  address1: string;
  address2: string;
  liquidity1: BigNumber;
  liquidity2: BigNumber;
  multiplier1?: BigNumber;
  multiplier2?: BigNumber;
  fee1: BigNumber;
  fee2?: BigNumber;
  inverted?: boolean;
}

interface Route {
  address1: string;
  address2: string;
  liquidity1: BigNumber;
  liquidity2: BigNumber;
  multiplier1?: BigNumber;
  multiplier2?: BigNumber;
  fee1: BigNumber;
  fee2: BigNumber;
  inverted?: boolean;

  dex: typeof DEXES;
  contractAddress: string;
  amountIn: BigNumber;
  amountOut: BigNumber;
  minAmountOut: BigNumber;
}

interface Dexes {
  dex: typeof DEXES;
  fromTokenImage: string;
  toTokenImage: string;
  fromTokenSymbol: string;
  toTokenSymbol: string;
  fromTokenAddress: string;
  toTokenAddress: string;
}

interface Price {
  route: Route[]; // TODO change to routes
  address1: string;
  address2: string; // TODO addressIn and addressOut?
  decimals1: string; // TODO decimals in and out?
  decimals2: string;
  amount: BigNumber;
  dex: Dexes[];
  price: BigNumber;
  priceWithDecimals: BigNumber;
}

export const getPrices = ({
  contractStorage,
  depth = 4,
  maxResults = MAX_RESULTS,
  includeAggregation = false,
}: {
  contractStorage: Map<string, Record<string, unknown>>;
  depth?: number;
  maxResults?: number;
  includeAggregation?: Boolean;
}) => ({
  fromTokenAddress,
  toTokenAddress,
  amount,
  slippageTolerance = 0,
}: {
  fromTokenAddress: string;
  toTokenAddress: string;
  amount: string;
  slippageTolerance: number;
}): Price[] => {
  const pools = normalizeStateToPool({
    contractStorage,
    contractAddressToDex,
  });

  const routes = getRoutes({
    pools,
    fromTokenAddress,
    toTokenAddress,
    depth,
  });

  const amountIn = new BigNumber(
    convertTokenToDecimals(fromTokenAddress, amount) || 0
  ); //FIXME 0

  let aggregatedRoutes = [];

  if (includeAggregation) {
    aggregatedRoutes = getAggregatedVariants({
      amountIn,
      routes,
      fromTokenAddress,
      toTokenAddress,
      slippageTolerance,
    });
  }
  const routesWithPrice = getPricesForRoutes(
    amountIn,
    routes,
    fromTokenAddress,
    toTokenAddress,
    slippageTolerance
  );

  const routesWithPriceSorted = routesWithPrice
    .concat(aggregatedRoutes)
    .sort((a: Price, b: Price) => b.price.minus(a.price).toFixed());

  return routesWithPriceSorted.splice(0, maxResults).map((el: Price) => ({
    ...el,
    priceWithDecimals: applyDecimalsToToken(el.address2, el.price.toString()),
  }));
};
