/* Before executing this file first install deps
 * npm i --no-save @taquito/taquito
 *
 * and then build the project with
 * npm run build
 *
 * and you are ready to go from the root folder
 * node docs/example.js
 */
const { TezosToolkit } = require('@taquito/taquito');
/* Importing all addresses supported by @pickyfi/toknes package 
 * alternatively you can specify which liquidity pools you want to load
 * either manually 

 const { DEXES } = require("@pickyfi/tokens");
 new Map(['KT1CY3dmjYmF1E9pj3P3fm2k93NA1h8yuvV7', DEXES.SPICYSWAP])

 * or filtering an existed list

 new Map([
   ...[...contractAddressToDex.entries()].filter(
     ([address, dex]) => dex !== "PLENTY"
   ),
 ]);
 */
const { contractAddressToDex } = require('@pickyfi/tokens');
const { getPrices, getRoutesCombinations } = require('../dist/index.js');
const { mockStorage } = require('./contractStorageMock.js');

const RPC_URL = 'https://mainnet.api.tez.ie';
/* If there is an issue with accessing node you can try different from list: 

 https://mainnet.tezos.marigold.dev/
 https://rpc.tzbeta.net/
 https://mainnet.smartpy.io
 https://eu01-node.teztools.net/

 * see full list at - https://tezostaquito.io/docs/rpc_nodes/
 */

const tezos = new TezosToolkit(RPC_URL);
/* 
 This is the very basic and very inefficient way of getting contracts data/
 You can use your own indexer or any other techniq for optimization to get them
 faster and reduce the load to the public RPC. As long as it has the the same shape
 such as Map([contractAddress, contractStorage]) in will work
*/
const getStorage = async () =>
  new Map([
    ...(await Promise.all(
      Array.from(contractAddressToDex.keys()).map(contract =>
        tezos.contract
          .at(contract)
          .then(dex => dex.storage())
          .then(storage => [contract, storage])
      )
    )),
  ]);

(async () => {
  /* use to experiment with hardcoded mock storage */
  const contractStorage = new Map(await getStorage());
  /* use to experiment with actual fetched storage */
  /* const contractStorage = new Map(mockStorage); */
  const prices = getPrices({
    contractStorage
  })({
    fromTokenAddress: 'KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV', // kUSD
    toTokenAddress: 'KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn', // tzBTC
    amount: '1000',
  });
  /* /* It returns 5 prices, can be configured by maxRouts parameter of getPrices*/
  /*  * use Infinity to set it to max*/
  /*  */
  const [bestPrice] = prices;
  const bestPriceRoute = bestPrice.dex.reduce((acc, dex, idx) => {
    acc += idx !== 0 ? ' -> ' : '';
    acc += `${dex.fromTokenSymbol}/${dex.toTokenSymbol}`;
    return acc;
  }, '');
  console.log(
    `best route is ${bestPriceRoute} with price ${bestPrice.priceWithDecimals}`
  );
})();
